import java.util.Scanner;
public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String artikel = liesString("Was m�chten Sie bestellen?", myScanner);
		int anzahl = liesInt("Geben Sie die Anzahl ein:", myScanner);
		double preis = liesDouble("Geben Sie den Nettopreis ein:", myScanner);
		double mwst = liesDouble ("Geben Sie den Mehrwertsteuersatz in Prozent ein:", myScanner);
		double nettogesamtpreis = berechneGesamtnettopreis (anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis (nettogesamtpreis, mwst);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		myScanner.close();
		
	}

		public static String liesString(String text, Scanner ms) {
		// Benutzereingaben lesen
		System.out.println(text);
		String artikel = ms.next();
		return artikel;
		}

		
		public static int liesInt(String text, Scanner ms) {
		
		System.out.println(text);
		int anzahl = ms.nextInt();
		return anzahl;
		}
		
		public static double liesDouble (String text, Scanner ms) {
	
		System.out.println(text);
		double preis = ms.nextDouble();
		return preis;
		}
		
		
		public static double berechneGesamtnettopreis (int anzahl, double preis) {
		
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
		}
		
		
		// Verarbeiten
		public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
		
		}
		
		// Ausgeben
		public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
		}
	
	
}
