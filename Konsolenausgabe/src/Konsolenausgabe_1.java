
public class Konsolenausgabe_1 {

	public static void main(String[] args) {
		System.out.println("Hallo!\nWie gehts dir?\tMir gehts gut!");
		System.out.println("\b Das ist gut");
		System.out.println("\"Das ist gut!\"");
		
		String z = "Java-Programm";
		
		System.out.printf(  "FI-B 11|%.4s|", z);
		
		int n = 123;
		
		System.out.printf("\n%d", n);
		
	
		double k = 12.93886;
		
	System.out.printf("\n%.3f", k);
		
	
	String a = "Aufgabe 1";
	String b = "Aufgabe 2";
	String y = "Aufgabe 3";
	
	String c = "Die Erklärungen von String und Int waren nicht ganz klar.";
	String d ="Nicht ganz klar waren die Erklärungen von String und Int.";
	String e = "*";
	String f = "***";
	String g = "*****";
	String h = "*******";
	String i = "*********";
	String j = "***********";
	String l = "*************";
	double m = 22.4234234;
	double r = 111.2222;
	double o = 4.0;
	double p = 1000000.551;
	double q = 97.34;
	
	
	
	System.out.printf("\n\n\n%s", a);
	System.out.printf("\n");
	System.out.printf("\n%s", c );
	System.out.printf("\t%s", d);
	System.out.printf("\n");
	System.out.printf("\n");
	System.out.printf("Er sagte: \"%s\"",c); 
//	Das ist ein Kommentar

	System.out.printf("\n\n\n%s", b);
	System.out.printf("\n");
	System.out.printf("\n");
	System.out.printf("%7s", e);
	System.out.printf("\n");
	System.out.printf("%8s", f);
	System.out.printf("\n");
	System.out.printf("%9s", g);
	System.out.printf("\n");
	System.out.printf("%10s", h);
	System.out.printf("\n");
	System.out.printf("%11s", i);
	System.out.printf("\n");
	System.out.printf("%12s", j);
	System.out.printf("\n");
	System.out.printf("%13s", l);
	System.out.printf("\n");
	System.out.printf("%8s", f);
	System.out.printf("\n");
	System.out.printf("%8s", f);
	
	
	System.out.printf("\n\n%s", y);
	System.out.printf("\n\n");
	System.out.printf("%.2f", m);
	System.out.printf("\n");
	System.out.printf("%.2f", r);
	System.out.printf("\n");
	System.out.printf("%.2f", o);
	System.out.printf("\n");
	System.out.printf("%.2f", p);
	System.out.printf("\n");
	
	System.out.printf("%.2f", q);
	System.out.println("Default");
	System.out.println("Default2");
	
	}

}
