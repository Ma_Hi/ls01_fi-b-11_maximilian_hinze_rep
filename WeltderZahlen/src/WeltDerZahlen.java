/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    byte anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstra�e
     long anzahlSterne = 250000000000l ;
    
    // Wie viele Einwohner hat Berlin?
      int bewohnerBerlin= 3664000 ;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       int alterTage = 10254;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm =  190000 ;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land der Erde hat?
       int flaecheGroessteLand = 17000000 ; 
    
    // Wie gro� ist das kleinste Land der Erde?
    
       float flaecheKleinsteLand = 0.44f ;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne + " (ca. 250 +/- 150 Mrd)");
    
    System.out.println("Einwohner Berlin: " + bewohnerBerlin);
    
    System.out.println("Mein Alter in Tagen: " + alterTage);
    
    System.out.println("Schwerstes Tier der Welt: Der Blauwal mit " + gewichtKilogramm + "kg");
    
    System.out.println("Das gr��te Land der Welt: Russland mit " + flaecheGroessteLand + "km�");
    
    System.out.println("Das kleinste Land der Welt: Vatikanstadt mit " + flaecheKleinsteLand + "km�");
    
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
