import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag ; 
       float eingezahlterGesamtbetrag;
       float eingeworfeneM�nze;
       float r�ckgabebetrag;
       int anzahlTickets;
       System.out.printf("Zu zahlender Betrag (Euro): ");
       zuZahlenderBetrag = tastatur.nextFloat();
       
       System.out.printf("Wie viele Ticktes m�chten Sie?: ");
       anzahlTickets = tastatur.nextInt();
       zuZahlenderBetrag= zuZahlenderBetrag * anzahlTickets;
       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0f;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   
    	   System.out.printf("\nNoch zu zahlen: %.2f Euro", zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag*anzahlTickets > 0.0)
       {
    	   //double e = r�ckgabebetrag; 
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro", r�ckgabebetrag);
    	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");
    	   System.out.println(anzahlTickets +" Tickets wurden gedruckt!");
    	   
           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 Euro");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 Euro");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 Cent");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 Cent");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 Cent");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 Cent");
 	          r�ckgabebetrag -= 0.05;
           }
           while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
           {
 	         System.out.println("2 Cent");
	          r�ckgabebetrag -= 0.02;
       	   }
           
       	   while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
       	   {
	          System.out.println("1 Cent");
 	          r�ckgabebetrag -= 0.01;
 	          
 	         tastatur.close();
           }
       }
       System.out.println("\n"+ anzahlTickets + " Fahrscheine wird ausgegeben");
       System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
       
    }
}