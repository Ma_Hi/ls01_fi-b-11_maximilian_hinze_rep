
public class Captain {

	// Attribute
	private String surname;
	private String name;
	private int captainYears;
	private double gehalt;
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.

	// Konstruktoren

	// und name initialisiert.

	// initialisiert.

	// Verwaltungsmethoden
	// TODO: 4. Implementieren Sie einen Konstruktor, der die Attribute surname
	public Captain(String surname, String name) {
		this.surname = surname;
		this.name = name;
	}

	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	public Captain(String name, String surname, int captainYears, double gehalt) {
		this(name, surname);
		setCaptainYears(captainYears);
		setSalary(gehalt);
	}

	public void setSurname () {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears. Ber�cksichtigen Sie, dass das Jahr nach Christus sein soll.
	public void setCaptainYears(int captainYears) {
		this.captainYears = captainYears;
		if (captainYears > 0) {
			this.captainYears = captainYears;
		} else {
			System.out.println(
					"Unser Kaptit�n ist �lter als Jesus?! Das kann nicht sein, er Erhalt der Lizenz wird auf 2022 gesetzt!");
			this.captainYears = 10;
		}
	}

	public int getCaptainYears() {
		return this.captainYears;
	}

	public void setSalary(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		this.gehalt = gehalt;
		if (gehalt > 0) {
			this.gehalt = gehalt;
		} else {
			System.out.println("Diese Gehalt ist eines Kapt�ins nicht w�rdig! Es wird auf 600000.0 gesetz!");
			this.gehalt = 600000.0;
		}
	}

	// TODO: 2. Implementieren Sie die entsprechende get-Methode.
	public double getSalary() {
		return gehalt;
	}

	// Weitere Methoden

	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String vollName() {
		return this.name + " " + this.surname;
	}

	// TODO: 8. Implementieren Sie die toString() Methode.
	public String toString() { // overriding the toString() method
		return "Unser Kapit�n hei�t " + name + " " + surname + " " + "und ist schon " + captainYears
				+ " Jahre lang im Dienst! " + "Unser Kapit�n verdient " + gehalt + " Space-Euro!";

	}

}