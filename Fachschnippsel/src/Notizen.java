import java.util.Scanner;
public class Notizen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		float zahl1 = 0.0f, zahl2 = 0.0f, summe =0.0f;
		int zahl3 = 0, zahl4 = 0;
		double zahlDouble;
		long zahlLong;
		byte zahlByte;
		short zahlShort;
		boolean wahrheitswert;
		char buchstabe;
		String name = " ";
		
		System.out.println("Das Programm addiert zwei Gleitkommazahlen zusammen\n");
		System.out.print("Bitte geben Sie die erste Zahl ein: ");
		zahl1 = myScanner.nextFloat();
		
		System.out.print("Bitte geben Sie die zweite Zahl ein: ");
		zahl2 = myScanner.nextFloat();
		
		summe = zahl1 + zahl2;
		
		System.out.println("Ihr Ergebnis lautet: " + summe);
		
		System.out.print("\nBitte geben sie Ihren Namen ein: ");
		name = myScanner.next();
		System.out.println("Der eingebene Name ist: " + name);
		
		System.out.println("\nDas Programm addiert zwei ganze Zahlen zusammen");
		System.out.print("\nBitte geben Sie die erste ganze Zahl ein:");
		zahl3 = myScanner.nextInt();
		
		System.out.print("Bitte geben Sie die zweite ganze Zahl ein: ");
		zahl4 = myScanner.nextInt();
		
		summe = zahl3 + zahl4;
		
		System.out.println("Ihr Ergebnis lautet: "+ summe);
		
		System.out.print("Double: ");
		zahlDouble = myScanner.nextDouble();
		System.out.println(zahlDouble);
		
		System.out.print("Short: ");
		zahlShort = myScanner.nextShort();
		System.out.println(zahlShort);
		
		System.out.print("Long: ");
		zahlLong = myScanner.nextLong();
		System.out.println(zahlLong);
		
		
		System.out.print("Byte: ");
		zahlByte = myScanner.nextByte();
		System.out.println(zahlByte);
		
		
		System.out.print("Boolean: ");
		wahrheitswert = myScanner.nextBoolean();
		System.out.println(wahrheitswert);
		
		
		System.out.print("Char: ");
		buchstabe = myScanner.next().charAt (0);
		System.out.println(buchstabe);
		
		
		myScanner.close();
		
		int c =10, d =5;
		char buch = 'f';
		boolean e = true;
		int a =2, g =3;
		
		
		System.out.println(c < 9 || d >= 11);
		
		System.out.println(c > 9 || (d > 7 && buch == 'f'));
		
		System.out.println((c < 5 || d < 11) && (buch != 'a' || c == 4));
		
		System.out.println(!(c==10) && d <= 11);
		
		System.out.println(!(c == 10));
		
		System.out.println(e);
		
		System.out.println(!(c != 10) && ((!(buch != 'f') && c < 11) && !e));
		
		
		System.out.println(a < g);
		
	}

}
