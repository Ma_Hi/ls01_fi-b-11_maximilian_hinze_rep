import java.util.Scanner;


class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       float zuZahlenderBetrag ;
       float r�ckgabebetrag;
       
       boolean schleife = true;
       while (schleife == true) { //while Schleife f�r die endlose Abfrage der gefragten Eingabe
    	   
       zuZahlenderBetrag = fahrkartenbestellungErfassen (tastatur);
       r�ckgabebetrag = fahrkartenBezahlen (zuZahlenderBetrag, tastatur);
       fahrkartenAusgeben();
       rueckgeldAusgeben (r�ckgabebetrag); 
       }
       tastatur.close();
       
    }
    
    public static float fahrkartenbestellungErfassen (Scanner myScanner) {
    	float preisDesTickets;
    	int zahlTickets;
    	String ausTicket;
    	char rechnung ='j';
    	float gesamt = 0f;
    	
    	while (rechnung == 'j') {
       System.out.println("W�hlen Sie eines der verf�gbaren Tickets: ");
       System.out.println("Einzelfahrt im Regeltarif AB 2,90� [AB, ab]");
       System.out.println("Tageskarte im Regeltarif AB 8,60� [ABT, abt]");
       System.out.println("Gruppentageskarte im Regeltarif AB 23,50� [ABG, abg]");
       
       ausTicket = myScanner.next();
       preisDesTickets = ticketWahl (ausTicket);
       
       System.out.printf("Wie viele Ticktes m�chten Sie? \nMind. 1, aber nicht mehr als 10!: ");
       zahlTickets = myScanner.nextInt();
       
       while (zahlTickets > 10 || zahlTickets < 1) { //Abfrage der Eingabe, so lange bis ein richtiger Wert eingeben wird. 
    	   System.out.println("Sie haben leider eine falsche Eingabe get�tigt, bitte geben Sie eine g�ltige Anzahl an! :) ");
    	   zahlTickets = myScanner.nextInt();
    	    
       }
       gesamt += zahlTickets*preisDesTickets;
       System.out.printf("Aktueller Preis %.2f�.", gesamt);
       System.out.println("\nM�chte Sie weitere Tickets? (j/n)"); 
       rechnung = myScanner.next().charAt(0);
    	}
    	return gesamt;
    }
   public static float ticketWahl (String tags) { //Schalter f�r die Eingabe von Strings gebaut
	   
	   switch (tags) {
	   
	   case "AB":
		   return 2.90f;
	   case "ab":
		   return 2.90f;
	   case "ABT":
		   return 8.60f;
	   case "abt":
		   return 8.60f;
	   case "ABG":
		   return 23.50f;
	   case "abg":
		   return 23.50f;
		   
	   }
	   return 0;
   }
       
       // Geldeinwurf
       // -----------
       
     public static float fahrkartenBezahlen (float kosten, Scanner myScanner) {
    	
    	 float eingezahlterGesamtbetrag = 0.0f;
    	 float eingeworfeneM�nze;
    
       while(eingezahlterGesamtbetrag < kosten) {
    	   System.out.printf("\nNoch zu zahlen: %.2f Euro", kosten - eingezahlterGesamtbetrag);
    	   System.out.printf("\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = myScanner.nextFloat();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
          }
       return eingezahlterGesamtbetrag - kosten; 
     }

       // Fahrscheinausgabe
       // -----------------
     
     public static void fahrkartenAusgeben () {
     
    	 System.out.println("Tickets wurden gedruckt!");
       
    	 for (int i = 0; i < 23; i++)
       {
          System.out.print("=*");
          
          
          try {
			Thread.sleep(180);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
     }

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
     public static void rueckgeldAusgeben (float r�ckgabebetrag ) {
    	 
       if(r�ckgabebetrag > 0.0) {
    	   //double e = r�ckgabebetrag; 
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro", r�ckgabebetrag);
    	   System.out.println("\nwird in folgenden M�nzen ausgezahlt:");
    	   
   
    	   
           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 Euro");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 Euro");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 Cent");
	          r�ckgabebetrag -= 0.5;
           }
           while(Math.round(r�ckgabebetrag*100.0f)/100.0f >= 0.2) // 20 CENT-M�nzen, Math.round f�r die Rundungsfehler vom Programm
           {
        	  System.out.println("20 Cent");
 	          r�ckgabebetrag -= 0.2;
           }
           while(Math.round(r�ckgabebetrag*100.0f)/100.0f >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 Cent");
	          r�ckgabebetrag -= 0.1;
           }
           while(Math.round(r�ckgabebetrag*100.0f)/100.0f >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 Cent");
 	          r�ckgabebetrag -= 0.05;
           }
           while(Math.round(r�ckgabebetrag*100.0f)/100.0f >= 0.02)// 2 CENT-M�nzen
           {
 	         System.out.println("2 Cent");
	          r�ckgabebetrag -= 0.02;
       	   }
           
       	   while(Math.round(r�ckgabebetrag*100.0f)/100.0f >= 0.01)// 1 CENT-M�nzen
       	   {
	          System.out.println("1 Cent");
 	          r�ckgabebetrag -= 0.01;
 	          
 	         
           }
       }
       System.out.println("\nVergessen Sie nicht, den/die Fahrschein/e\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.\n");
       
    }
}
