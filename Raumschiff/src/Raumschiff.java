import java.util.ArrayList;
public class Raumschiff {

		private int anzP_Torpedos;
		private int energyLv;
		private int shieldLv; 
		private int shellLv;
		private int lifesysLv;
		private int anzAndroid;
		private String shipName; 
		ArrayList<Ladung> cargo = new ArrayList<Ladung>();
		ArrayList<String> bcComm = new ArrayList<String>();
		
		public Raumschiff () {
		
		}
		public Raumschiff (int anzP_Torpedos, int energyLv, int shieldLv, int shellLv, int lifesysLv, int anzAndroid, String shipName) {
			this.anzP_Torpedos = anzP_Torpedos;
			this.energyLv = energyLv;
			this.shieldLv = shieldLv; 
			this.shellLv = shellLv;
			this.lifesysLv = lifesysLv;
			this.anzAndroid = anzAndroid;
			this.shipName = shipName;
		}
		public int getAnzP_Torpedos () {
			return this.anzP_Torpedos;
		}
		public void setAnzP_Torpedos (int anzP_Torpedos) {
			this.anzP_Torpedos = anzP_Torpedos;
		}
		public int getEnergyLv () {
			return this.energyLv;
		}
		public void setEnergyLv (int energyLv) {
			this.energyLv = energyLv;
		}
		public int getShieldLv () {
			return this.shieldLv;
		}
		public void setShieldLv (int shieldLv) {
			this.shieldLv = shieldLv;
		}
		public int getShellLv () {
			return this.shellLv;
		}
		public void setShellLv (int shellLv) {
			this.shellLv = shellLv;
		}
		public int getLifesysLv () {
			return this.lifesysLv;
		}
		public void setLifesysLv (int lifesysLv) {
			this.lifesysLv = lifesysLv;
		}
		public int getAnzAndroid () {
			return this.anzAndroid;
		}
		public void setAnzAndroid (int anzAndroid) {
			this.anzAndroid = anzAndroid;
		}
		public String getShipName () {
			return this.shipName;
		}
		public void setShipName (String shipName) {
			this.shipName = shipName;
		}
		public void addLadung (Ladung item) {
			cargo.add (item);
		}
		//public static displayLogBook ArrayList<String> () {
		//
		//}
		public void shootP_Torpedos (Raumschiff spaceShip) {
			System.out.println("\n*Phew* Get some! *Phew*");
			if (this.anzP_Torpedos > 0) 
			{
			messageAll("Das Ziel " + spaceShip.shipName + " wurde von einem Photonentorpedo getroffen!");
			this.anzP_Torpedos--;
			spaceShip.energyLv = spaceShip.energyLv - 50; 
			hitScan(spaceShip);
			}
			else 
			{
			messageAll("-=*Click*=-");
			messageAll("Oh no!");
			}

		}
		public void shootP_Canon (Raumschiff spaceShip) {

		}
		private void hitScan (Raumschiff spaceShip) {
			if (spaceShip.energyLv > 1) {
			System.out.println("Das Engerylevel von " + spaceShip.shipName + " betr�gt nur noch "+ spaceShip.energyLv +" Prozent!");
			}
			else 
			{
			System.out.println(spaceShip.shipName + " goes dooown!");
			}
		}
		public void messageAll (String message) {
			System.out.println (message);
		}
		public void shipStatus () {
			System.out.println("\n//////////////////////////////////////////////////////////");
			System.out.println("Ein Statusbericht wird geladen:");
			System.out.println("-Das Raumschiff " + shipName + " hat folgende Eigenschaften: ");
			System.out.println("-Es wurde(n) " + this.anzP_Torpedos + " Photonentorpedo(s) geladen!");
			System.out.println("-Das Energielevel liegt bei " + this.energyLv + " Prozent!");
			System.out.println("-Die Schilde sind zu " + this.shieldLv + " Prozent aufgeladen!");
			System.out.println("-Die H�lle ist zu " + this.shellLv + " Prozent intakt!");
			System.out.println("-Die Kapazit�t der Lebenserhaltungssysteme liegt bei " + this.lifesysLv + " Prozent!");
			System.out.println("-Es stehen "+ this.anzAndroid +" Androiden zur Reparaturen bereit!");
			System.out.println("Alle Systeme sind betriebsbereit und auf GO!");
			
		}
		public void cargoList () {
			System.out.println("_________________________________________________");
			System.out.println("Ladungsverzeichnis von "+ this.shipName + ": ");
			System.out.println(cargo);
		}
}




