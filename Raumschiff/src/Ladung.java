
public class Ladung {

	private String cargoName;
	private int amount;
	
	public Ladung (String cargoName, int amount) {
		this.cargoName = cargoName;
		this.amount = amount;
	}
	public String getCargoName () {
		return this.cargoName;
	}
	public void setCargoName (String cargoName) {
		this.cargoName = cargoName;
	}
	public int getAmount () {
		return this.amount;
	}
	public void setAmount (int amount) {
		this.amount = amount;
	}
	@Override
	public String toString () {
		return cargoName + " " + amount;
	}
}

