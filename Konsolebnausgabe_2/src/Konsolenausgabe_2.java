
public class Konsolenausgabe_2 {

	public static void main(String[] args) {
		String a = "**";
		String b = "*";
		System.out.printf("Aufgabe 1\n\n");
		System.out.printf("%6s", a);
		System.out.printf("\n%1s", b);
		System.out.printf("%9s", b);
		System.out.printf("\n%1s", b);
		System.out.printf("%9s", b);
		System.out.printf("\n%6s", a);

		
		System.out.printf("\n\n");
		
		String one = "0";
		int two = 1;
		int three = 2;
		int four = 3;
		int five = 4;
		int six = 5;
		int seven = 1;
		int eight = 2;
		int nine = 6;
		int ten = 24;
		int eleven = 120;
				
		String em = "!";
		String x = "*";
		String eq = "=";
		String eq2 = " =";
		System.out.printf("Aufgabe 2\n");
		System.out.printf("\n%1s", one);
		System.out.printf(em);
		System.out.printf("%3s", eq); System.out.printf("%20s", eq2); System.out.printf("%4s", two);
		System.out.printf("\n%1s", two);
		System.out.printf(em);
		System.out.printf("%3s", eq); System.out.printf("%2s", two); System.out.printf("%18s", eq2); System.out.printf("%4s", two);
		System.out.printf("\n%1s", three);
		System.out.printf(em);
		System.out.printf("%3s", eq); System.out.printf("%2s", two); System.out.printf("%2s", x); System.out.printf("%2s", three); System.out.printf("%14s", eq2); System.out.printf("%4s", three);
		System.out.printf("\n%1s", four);
		System.out.printf(em);
		System.out.printf("%3s", eq); System.out.printf("%2s", two); System.out.printf("%2s", x); System.out.printf("%2s", three); System.out.printf("%2s", x); System.out.printf("%2s", four); System.out.printf("%10s", eq2); System.out.printf("%4s", nine);
		System.out.printf("\n%1s", five);
		System.out.printf(em);
		System.out.printf("%3s", eq); System.out.printf("%2s", two); System.out.printf("%2s", x); System.out.printf("%2s", three); System.out.printf("%2s", x); System.out.printf("%2s", four); System.out.printf("%2s" , x); System.out.printf("%2s", five); System.out.printf("%6s", eq2); System.out.printf("%4s", ten);
		System.out.printf("\n%1s", six);
		System.out.printf(em);
		System.out.printf("%3s", eq); System.out.printf("%2s", two); System.out.printf("%2s", x); System.out.printf("%2s", three); System.out.printf("%2s", x); System.out.printf("%2s", four); System.out.printf("%2s" , x); System.out.printf("%2s", five); System.out.printf("%2s" , x); System.out.printf("%2s", six); System.out.printf("%s", eq2); System.out.printf("%4s", eleven);
		System.out.printf("\n");
		
		System.out.printf("\nAufgabe 3\n\n");
		
		String F = "Fahrenheit";
		String C = "Celsius";
		String B = "|"; 
		String L = "----------------------"; //22 Lines
		int aa = 20; 
		int ab = 10;
		String ac = "+0";
		int ad = 10 ;
		int ae = 20 ;
		double ba = -28.8889;
		double bb = -23.3333;
		double bc = -17.7778;
		double bd = -6.6667;
		double be = -1.1111;
		
		
		
		System.out.printf(F); System.out.printf("%2s", B); System.out.printf("%10s", C);
		System.out.printf("\n%1s", L);
		System.out.printf("\n%1s", -aa); System.out.printf("%9s", B); System.out.printf("%10.2f", ba);
		System.out.printf("\n%1s", -ab); System.out.printf("%9s", B); System.out.printf("%10.2f", bb);
		System.out.printf("\n%1s", ac); System.out.printf("%10s", B); System.out.printf("%10.2f", bc);
		System.out.printf("\n%+-1d", ad); System.out.printf("%9s", B); System.out.printf("%10.2f", bd);
		System.out.printf("\n%+-1d", ae); System.out.printf("%9s", B); System.out.printf("%10.2f", be);
		
		
		
		
	}

}
